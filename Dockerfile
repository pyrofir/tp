FROM golang:1.8-alpine
RUN mkdir /app 
COPY . /app/ 
WORKDIR /app 
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
RUN addgroup -S appgroup && adduser -S appuser -G appgroup
HEALTHCHECK NONE
USER 1000
CMD ["/app/main"]
EXPOSE 80
